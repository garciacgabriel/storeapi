package br.edu.unisep.store.domain.builder;


import br.edu.unisep.store.data.entity.Product;
import br.edu.unisep.store.domain.dto.ProductDto;
import br.edu.unisep.store.domain.dto.RegisterProductDto;

import java.util.List;
import java.util.stream.Collectors;

public class ProductBuilder {
    public final String statusDescription0 = "Inativo";
    public final String statusDescription1 = "Ativo";
    public final String statusDescription2 = "Suspenso";

    public String getStatusDescription(Product product) {
        if (product.getStatus() == 0) {
            return statusDescription0;

        } else if (product.getStatus() == 1) {
            return statusDescription1;

        } else {
            return statusDescription2;
        }
    }

    public List<ProductDto> from(List<Product> products) {
        return products.stream().map(this::from).collect(Collectors.toList());
    }

    public ProductDto from(Product product) {

        if (product != null) {
            return new ProductDto(
                    product.getId(),
                    product.getName(),
                    product.getDescription(),
                    product.getPrice(),
                    product.getBrand(),
                    product.getStatus(),
                    this.getStatusDescription(product)

            );
        }

        return null;
    }

    public Product from(RegisterProductDto registerProduct) {
        var product = new Product();

        product.setName(registerProduct.getName());
        product.setDescription(registerProduct.getDescription());
        product.setPrice(registerProduct.getPrice());
        product.setBrand(registerProduct.getBrand());
        product.setStatus(registerProduct.getStatus());

        return product;
    }
}
